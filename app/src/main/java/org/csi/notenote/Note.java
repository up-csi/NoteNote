package org.csi.notenote;

/**
 * Created by Nigel on 9/12/2015.
 */
public class Note {
    public int id;
    public String title;
    public String content;

    public Note() {
    }

    public Note(String title, String content) {
        super();
        this.title = title;
        this.content = content;
    }
}
