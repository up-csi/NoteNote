package org.csi.notenote;

import android.provider.BaseColumns;

/**
 * Created by Nigel on 9/12/2015.
 */
public class NoteContract {

    public NoteContract() { }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CONTENT = "content";
    }

}
